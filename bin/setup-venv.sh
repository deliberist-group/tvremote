#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

readonly project_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
readonly venv_dir="${project_dir}/.venv"

# Create a new Python venv.
if [[ ! -d "${venv_dir}" ]] ; then
    python3 -m venv "${venv_dir}"
fi
source "${venv_dir}/bin/activate"

# TODO do this in a "requirements.txt" file?
python3 -m pip install nodeenv

# Create a new NodeJS venv, but "append" it to the Python venv.
if [[ ! -d "${venv_dir}/lib/node_modules" ]] ; then
    python3 -m nodeenv --python-virtualenv
fi

# TODO create some sort of npm equivalent to a "requirements.txt" file?
npm install --global --no-save npm gitlab-ci-local
