import globals from "globals";

export default [
  {
    languageOptions: {
      ecmaVersion: "latest",
      globals: {
        ...globals.browser,
      },
      sourceType: "script",
    },
  }
];
