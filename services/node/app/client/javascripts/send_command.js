const buttons = document.querySelectorAll("button");
for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener('click', function () {
    fetch('/button_pressed/' + REMOTE, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        button_text: this.textContent
      })
    });
  });
}
