const express = require('express');
const lirc = require('../controllers/lirc');
const router = express.Router();
const Stopwatch = require('../lib/stopwatch');

const DEFAULT_REMOTE = sanitize_remote('AMAZON_L5B83H');

/**
 * Sanitizes the remote name as that comes from user input.  The sanitized
 * version will only contain underscores, dashes, and alpha-numeric characters.
 *
 * @param remoteName The name of the remote to use.
 *
 * @returns {string}
 */
function sanitize_remote(remoteName) {
  return remoteName.replace(/[^A-Za-z0-9_-]/gim, '');
}

// Render the remotes.
router.get('/', function (req, res, next) {
  const sw = new Stopwatch();
  try {
    // TODO get this dynamically
    const remotes = [
      'AMAZON_L5B83H',
      'LG_AKB74915324',
    ];
    res.render('../views/index', {
      title: 'Chose a TV Remote',
      remotes: remotes,
    });
  } finally {
    sw.log('page rendering', sw.MILLI);
  }
});

router.get('/remote', function (req, res, next) {
  res.redirect('/remote/' + DEFAULT_REMOTE);
});

router.get('/remote/:remote', function (req, res, next) {
  const sw = new Stopwatch();
  try {
    const remote = sanitize_remote(req.params.remote);
    res.render('../../remotes/' + remote + '/view', {
      title: 'TV Remote: ' + remote,
      remote: remote,
    });
  } finally {
    sw.log('page rendering', sw.MILLI);
  }
});

// Each time a button is pressed the button text is sent to the LIRC module for
// processing.
router.post('/button_pressed/:remote', function (req, res) {
  // Pull the information from the request ASAP, then end the response.
  // const referrer = req.body.referrer.toString();
  const sw = new Stopwatch();
  let button_text = null;
  try {
    button_text = req.body.button_text.toString();
    res.end();
  } finally {
    sw.log('ending response')
  }

  sw.restart();
  try {
    const remote = sanitize_remote(req.params.remote);
    lirc.send_code(remote, button_text);
  } finally {
    sw.log('lirc', sw.MILLI);
  }
});

// Export the router object.
module.exports = router;
