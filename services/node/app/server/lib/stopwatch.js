// todo docs

const process = require('process');

const _SEC_TO_NS = 1e9;

const _NANO_TO_UNIT = {
    'nano': 1,
    'micro': 1e3,
    'milli': 1e6,
    'centi': 1e7,
    'deci': 1e8,
    null: 1e9
};

// todo docs
var method = Stopwatch.prototype;

// todo docs
function Stopwatch() {
    // this._last_started = null;
    this.NANO = 'nano';
    this.MICRO = 'micro';
    this.MILLI = 'milli';
    this.CENTI  = 'centi';
    this.DECI = 'deci';
    this.restart();
}

// todo docs
method.restart = function () {
    this._last_started = process.hrtime();
};

// todo docs
method.stop = function (unit) {
    if (this._last_started === null){
        throw 'Cannot stop a stopwatch that has not been started.';
    }

    const end = process.hrtime(this._last_started);
    const ns = (end[0] * _SEC_TO_NS) + end[1];
    return _normalize(ns, unit);
};

// todo docs
method.log = function (name, unit) {

    const stop_time = this.stop(unit).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if (typeof(name) === 'undefined' || name === null) {
        name = '.';
    } else {
        name = ', for: ' + name + '.'
    }

    if (!(unit in _NANO_TO_UNIT)) {
        unit = null;
    }

    if (typeof(unit) === 'undefined' || unit === null) {
        unit = ' seconds';
    } else {
        unit = ' ' + unit + '-seconds'
    }

    console.log('Elapsed time: %s%s%s', stop_time, unit, name);
};

// todo
function _normalize(nanoseconds, unit) {
    if (!(typeof(unit) === 'undefined' || unit === null)) {
        unit = unit.toLowerCase();
    }

    if (!(unit in _NANO_TO_UNIT)) {
        // console.log('Unexpected metric unit: %s, defaulting to no metric units.', unit);
        unit = null;
    }

    return nanoseconds / _NANO_TO_UNIT[unit];
}

module.exports = Stopwatch;
