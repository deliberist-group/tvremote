#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

# BASE_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)"
# cd "${BASE_DIR}"

function print_header() {
    echo
    echo '================================================================================'
    HEADER=$@
    [[ ! -z ${HEADER} ]] && echo ${HEADER}
}

###############################################################################
print_header 'Building TLS configuration.'
openssl_conf_file="/etc/openssl.ini"
days_valid=3650
key_size="$(awk -F "=" '/default_bits/ {print $2}' ${openssl_conf_file} | tr -d ' ')"
pki_dir="/usr/local/etc/pki"
csr_file="${pki_dir}/csr.pem"
key_file="${pki_dir}/key.pem"
cert_file="${pki_dir}/cert.pem"

################################################################################
print_header 'Setting up TLS environment.'
mkdir -pv "${pki_dir}"

################################################################################
if [[ -f "${csr_file}" || -f "${key_file}" ]]; then
    print_header 'WARN: Not building a new CSR/key since they already exist.'
else
    print_header 'Building a new CSR.'
    openssl req                         \
        -config "${openssl_conf_file}"  \
        -nodes                          \
        -new                            \
        -newkey "rsa:${key_size}"       \
        -out "${csr_file}"              \
        -keyout "${key_file}"
fi

################################################################################
if [[ -f "${cert_file}" ]] ; then
    print_header 'WARN: Not self signing CSR since certificate already exists.'
else
    print_header 'Self-signing the CSR into a certificate.'
    openssl x509                \
        -req                    \
        -days "${days_valid}"   \
        -in "${csr_file}"       \
        -signkey "${key_file}"  \
        -out "${cert_file}"
fi

################################################################################
print_header 'Printing certificate.'
openssl x509 -text -noout -in "${cert_file}"
