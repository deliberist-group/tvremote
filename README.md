# tvremote

As the proud owner of ~~a dog that loves to chew on~~ children that break my
smart TV's remote, it clearly became a wise investment of time to create a
web-based TV remote that emulates the same functionality of the original TV
remote.  All buttons included.

## Hardware

This app should run on almost any Linux system.  For me, I installed the app on
a Raspberry Pi Zero W with the
[Irdroid USB IR Transceiver](http://www.irdroid.com/irdroid-usb-ir-transceiver/).

## Support for Multiple Remotes

The LG_AKB74915324 smart remote is an example of what needs to be done to
support multiple remotes.

- `./services/lirc/etc/lirc.conf.d/*.conf` contains the configuration files that
    will be combined together to for a single lircd config file.  Each file
    defines the [LIRC](http://www.lirc.org/) configuration that sends the RF
    codes to the IR transceiver.

- `./services/node/app/remotes/*/lirc.js` defines a mapping between button text
    from remotes `view.pug` to the codes used in the remote's
    `lirc.conf.d/*.conf` file.

- `./services/node/app/remotes/*/style.css` any additional styling needed to
    make the web view _look_ like the real remote.

- `./services/node/app/remotes/*/view.pug` outlines the web layout that
    simulates the look (and feel?) of the remote.
